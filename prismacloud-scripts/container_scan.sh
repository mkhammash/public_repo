#!/bin/bash
#######container_scan.sh - Container scan###########
if [[ "$1" != "" ]];then
    image=$1
    echo "image name:"$image
else
    echo "Please enter the image name to be scanned as an argument to bash script."
    exit 1;
fi

mkdir -p securethecloud
docker save $image -o securethecloud/image.tar
docker load -i securethecloud/image.tar
curl -u $prisma_cloud_compute_username:$prisma_cloud_compute_password --output ./twistcli $prisma_cloud_compute_url/api/v1/util/twistcli
chmod +x ./twistcli
./twistcli --version
IMAGEID=`docker images $image --format "{{.ID}}"`
./twistcli images scan --details --address $prisma_cloud_compute_url -u $prisma_cloud_compute_username -p $prisma_cloud_compute_password $IMAGEID
if [ "$?" == "1" ]; then
  exit 1;
fi